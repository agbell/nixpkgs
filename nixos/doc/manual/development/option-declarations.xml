<section xmlns="http://docbook.org/ns/docbook"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        xmlns:xi="http://www.w3.org/2001/XInclude"
        version="5.0"
        xml:id="sec-option-declarations">

<title>Option Declarations</title>

<para>An option declaration specifies the name, type and description
of a NixOS configuration option.  It is illegal to define an option
that has not been declared in any module. A option declaration
generally looks like this:

<programlisting>
options = {
  <replaceable>name</replaceable> = mkOption {
    type = <replaceable>type specification</replaceable>;
    default = <replaceable>default value</replaceable>;
    example = <replaceable>example value</replaceable>;
    description = "<replaceable>Description for use in the NixOS manual.</replaceable>";
  };
};
</programlisting>

</para>

<para>The function <varname>mkOption</varname> accepts the following arguments.

<variablelist>

  <varlistentry>
    <term><varname>type</varname></term>
    <listitem>
      <para>The type of the option (see below).  It may be omitted,
      but that’s not advisable since it may lead to errors that are
      hard to diagnose.</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>default</varname></term>
    <listitem>
      <para>The default value used if no value is defined by any
      module.  A default is not required; in that case, if the option
      value is ever used, an error will be thrown.</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>example</varname></term>
    <listitem>
      <para>An example value that will be shown in the NixOS manual.</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>description</varname></term>
    <listitem>
      <para>A textual description of the option, in DocBook format,
      that will be included in the NixOS manual.</para>
    </listitem>
  </varlistentry>

</variablelist>

</para>

<para>Here is a non-exhaustive list of option types:

<variablelist>

  <varlistentry>
    <term><varname>types.bool</varname></term>
    <listitem>
      <para>A Boolean.</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>types.int</varname></term>
    <listitem>
      <para>An integer.</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>types.str</varname></term>
    <listitem>
      <para>A string.</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>types.lines</varname></term>
    <listitem>
      <para>A string.  If there are multiple definitions, they are
      concatenated, with newline characters in between.</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>types.path</varname></term>
    <listitem>
      <para>A path, defined as anything that, when coerced to a
      string, starts with a slash.  This includes derivations.</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>types.package</varname></term>
    <listitem>
      <para>A derivation (such as <literal>pkgs.hello</literal>) or a
      store path (such as
      <filename>/nix/store/1ifi1cfbfs5iajmvwgrbmrnrw3a147h9-hello-2.10</filename>).</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>types.listOf</varname> <replaceable>t</replaceable></term>
    <listitem>
      <para>A list of elements of type <replaceable>t</replaceable>
      (e.g., <literal>types.listOf types.str</literal> is a list of
      strings).  Multiple definitions are concatenated together.</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>types.attrsOf</varname> <replaceable>t</replaceable></term>
    <listitem>
      <para>A set of elements of type <replaceable>t</replaceable>
      (e.g., <literal>types.attrsOf types.int</literal> is a set of
      name/value pairs, the values being integers).</para>
    </listitem>
  </varlistentry>

  <varlistentry>
    <term><varname>types.nullOr</varname> <replaceable>t</replaceable></term>
    <listitem>
      <para>Either the value <literal>null</literal> or something of
      type <replaceable>t</replaceable>.</para>
    </listitem>
  </varlistentry>

</variablelist>

You can also create new types using the function
<varname>mkOptionType</varname>.  See
<filename>lib/types.nix</filename> in Nixpkgs for details.

An option declaration must follow the following rules:
<itemizedlist mark='bullet'>

  <listitem>
    <para>A <varname>defaultText</varname> must be defined if and only if the type of the option
    derives from <varname>package</varname>, <varname>packageSet</varname> or <varname>nixpkgsConfig
    </varname>, and if and only if a <varname>default</varname> attribute is defined and if and only if
    the value of the <varname>default</varname> attribute is not the default of the type of the
    option declaration.

    For example, a <varname>defaultText</varname> must be defined for
<programlisting>
type = types.listOf types.package;
default = [ pkgs.foo; ];
defaultText = "[ pkgs.foo; ]";
</programlisting>.

    But no <varname>defaultText</varname> must be defined for
<programlisting>
type = types.listOf types.package;
default = [];
</programlisting>,
    as <varname>[]</varname> is the default of <varname>types.listOf types.package</varname>.
    </para>
  </listitem>

  <listitem>
    <para>A <varname>defaultText</varname> can be defined if the type of the option derives from
    <varname>path</varname> and if a <varname>default</varname> attribute is defined.</para>
  </listitem>

  <listitem>
    <para>The value of the <varname>example</varname> attribute must be wrapped with <varname>
    literalExample</varname> if the type of the option derives from <varname>package</varname>,
    <varname>packageSet</varname> or <varname>nixpkgsConfig</varname>.</para>
  </listitem>

  <listitem>
    <para>The value of <varname>defaultText</varname> and <varname>literalExample</varname> must
    be a string which contains a valid nix expression. The nix expression has to evaluate in
    <code>{pkgs}: <replaceable>value</replaceable></code>.
    For example:
    <itemizedlist>
      <listitem>
	<para>A <varname>defaultText</varname> could, e.g., be:
<programlisting>
type = types.package;
default = pkgs.foo;
defaultText = "pkgs.foo";
</programlisting>
        But not <code>defaultText = "pkgs.foo;";</code>, as that
        corresponds to <code>{pkgs}: pkgs.foo;</code>, which is an
	invalid nix expression due to the ending with <varname>;</varname>.</para>
      </listitem>
    
      <listitem>
	<para>A <varname>literalExample</varname> could be used as, e.g.:
<programlisting>
type = types.path;
example = literalExample "\"\${pkgs.bar}/bin/bar\"";
</programlisting>
        But not <code>literalExample "\${pkgs.bar}/bin/bar";</code>, as that corresponds
	to <code>{pkgs}: ${pkgs.bar}/bin/bar</code>, which is an invalid nix expression
	as the <varname>path</varname> is not a <varname>string</varname> anymore.</para>
      </listitem>
    </itemizedlist></para>
  </listitem>

  <listitem>
    <para>The <varname>type</varname> attribute must be defined for every option declaration.</para>
  </listitem>

</itemizedlist>

NixOS ships the tool <varname>nixos-typecheck</varname> that can check an option declaration to:

<itemizedlist mark='bullet'>

  <listitem>
    <para>Enforce that an option declaration has a <varname>defaultText</varname> if and only if the
    type of the option derives from <varname>package</varname>, <varname>packageSet</varname> or
    <varname>nixpkgsConfig</varname> and if a <varname>default</varname> attribute is defined.</para>
  </listitem>

  <listitem>
    <para>Enforce that the value of the <varname>example</varname> attribute is wrapped with <varname>
    literalExample</varname> if the type of the option derives from <varname>package</varname>,
    <varname>packageSet</varname> or <varname>nixpkgsConfig</varname>.</para>
  </listitem>

  <listitem>
    <para>Warn if a <varname>defaultText</varname> is defined in an option declaration if the type of
    the option does not derive from <varname>package</varname>, <varname>packageSet</varname> or
    <varname>nixpkgsConfig</varname>.</para>
  </listitem>

  <listitem>
    <para>Warn if no <varname>type</varname> is defined in an option declaration.</para>
  </listitem>

</itemizedlist></para>

</section>
